# Vacinação Covid-19

Análise e visualização dos dados da vacinação.

Uma iniciativa do [Laboratório de Estatística e Ciência de Dados - LED](https://im.ufal.br/laboratorio/led/).

# Instalação

Baixe ou clone o repositório. Na pasta do painel-vacina:

    $ python3 -m venv venv
    $ . venv/bin/activate
    $ pip install -r requirements.txt
    

# Carregando dados no banco

* é preciso ter uma instalação do postgresql (testado na 9.5)
    * usuário: usr_vacinas 
    * senha: sovacinasalva
    * porta:5432
    * banco: vacinas
* Rodar:
    * sh run_extract.sh (baixa os dados do opendatasus... demora bastante)
    * sh run_setup.sh
    * sh run_load.sh
    * sh run_clean.sh
