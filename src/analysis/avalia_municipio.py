#!/usr/bin/env python
# coding: utf-8

# In[2]:


from sqlalchemy import create_engine
import os
from os import listdir
from os.path import isfile, join
from datetime import datetime
import pandas as pd
import numpy as np
import geopandas
from geobr import read_municipality
import seaborn as sns; sns.set()

pd.set_option('display.max_rows', 6000)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 1000)

from settings import *


# In[3]:


engine = create_engine(connection_uri)


# In[13]:


sql = """
SELECT V.cod, V.vacina_dataaplicacao, V.total, 100000*V.total::NUMERIC/P.populacao::NUMERIC AS total_per100k FROM (
	(
		SELECT 0 AS cod, vacina_dataaplicacao, COUNT(*) AS total FROM public.vacinacao_limpa
		GROUP BY cod, vacina_dataaplicacao
		ORDER BY cod, vacina_dataaplicacao
	)
	UNION ALL 
	(
		SELECT SUBSTRING(estabelecimento_municipio_codigo::TEXT, 0, 3)::INTEGER AS cod, vacina_dataaplicacao, COUNT(*) AS total FROM public.vacinacao_limpa
		GROUP BY cod, vacina_dataaplicacao
		ORDER BY cod, vacina_dataaplicacao
	)
	UNION ALL (
		SELECT estabelecimento_municipio_codigo::INTEGER AS cod, vacina_dataaplicacao, COUNT(*) AS total FROM public.vacinacao_limpa
		GROUP BY cod, vacina_dataaplicacao
		ORDER BY cod, vacina_dataaplicacao
	)
) V JOIN populacao_total P ON (V.cod = P.cod)

;
"""

df_vac = pd.read_sql(sql, engine)

df_vac


# In[6]:





# In[ ]:




