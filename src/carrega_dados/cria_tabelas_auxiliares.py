#!/usr/bin/env python
# coding: utf-8

# In[1]:


# !pip install psycopg2
from sqlalchemy import create_engine
import os
from os import listdir
from os.path import isfile, join
from datetime import datetime
import pandas as pd
import numpy as np

from settings import *


# In[2]:


data_dir = os.path.join(os.path.abspath(''), '../../datasets/raw/')

pop_data_dir = os.path.join(os.path.abspath(''), '../../datasets/datasus/')


# In[3]:


df_pop = pd.read_csv(pop_data_dir + 'faixas_niveis_2020.csv', sep=';')

df_pop


# In[4]:


df_pop_total_m = df_pop.groupby(['nivel', 'CodEst', 'CodMun']).sum().sum(axis=1).reset_index(name='populacao')[['CodMun', 'populacao']]

df_pop_total_m.columns=['cod', 'populacao']

df_pop_total_m['cod'] = df_pop_total_m['cod'].astype(int)

df_pop_total_m


# In[5]:


df_pop_total_m.populacao.sum()


# In[6]:


df_pop_total_e = df_pop.query("nivel=='E'").drop(['CodMun', 'Sexo'], axis=1).groupby(['nivel', 'CodEst']).sum().sum(axis=1).reset_index(name='populacao')[['CodEst', 'populacao']]

df_pop_total_e.columns=['cod', 'populacao']

df_pop_total_e['cod'] = df_pop_total_e['cod'].astype(int)

df_pop_total_e


# In[7]:


print(len(df_pop_total_e), df_pop_total_e.populacao.sum())


# In[8]:


df_pop_total_n = df_pop.query("nivel=='N'").drop(['CodEst', 'CodMun', 'Sexo'], axis=1).groupby(['nivel']).sum().sum(axis=1).reset_index(name='total')[['total']]

df_pop_total_n['cod'] = 0

df_pop_total_n.columns=['populacao', 'cod']

df_pop_total_n = df_pop_total_n[['cod', 'populacao']]

df_pop_total_n['cod'] = df_pop_total_n['cod'].astype(int)

df_pop_total_n


# In[9]:


df_pop_total = pd.concat([df_pop_total_n, df_pop_total_e, df_pop_total_m])

df_pop_total


# In[10]:


engine = create_engine(connection_uri)


# In[11]:


df_pop_total.to_sql(
    name='populacao_total',
    con=engine,
    if_exists='replace',
    index=False)


# In[12]:


df_pop.to_sql(
    name='populacao',
    con=engine,
    if_exists='replace',
    index=False)


# In[37]:


ibge_data_dir = os.path.join(os.path.abspath(''), '../../datasets/ibge/')

df_nomes = pd.read_csv(ibge_data_dir + 'municipio_latlon.csv')

# df_nomes = df_nomes[df_nomes['capital'] == 0]

df_nomes


# In[38]:


df_nomes = df_nomes[['codigo_ibge', 'nome', 'capital', 'codigo_uf', 'latitude', 'longitude']]

df_nomes.columns = ['codigo_ibge', 'municipio', 'capital', 'codigo_uf', 'lat', 'lon']

df_nomes['codmun'] = df_nomes['codigo_ibge'].astype('int').astype('str').str[:6].astype('int')

df_nomes


# In[39]:


df_estados = pd.read_csv(ibge_data_dir + 'estados.csv')

df_estados.columns = ['codigo_uf', 'estado', 'estado_sigla']

df_estados['estado_sigla'] = df_estados['estado_sigla'].str.strip()

df_estados


# In[40]:


df_nomes = df_nomes.merge(df_estados)

df_nomes


# In[41]:


df_nomes.to_sql(
    name='municipios',
    con=engine,
    if_exists='replace',
    index=False)


# In[42]:


df_estados.to_sql(
    name='estados',
    con=engine,
    if_exists='replace',
    index=False)


# In[ ]:




