import pandas as pd

COLUNAS_COMPLETA = [
    {'nome': 'paciente_id', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'paciente_idade', 'tipo': 'int', 'tipo_pandas': pd.Int64Dtype(), 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'paciente_dataNascimento', 'tipo': 'date', 'tipo_pandas': 'date', 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'paciente_enumSexoBiologico', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'paciente_racacor_codigo', 'tipo': 'int', 'tipo_pandas': 'float', 'categorica': 0, 'distinct': 0, 'trim': 0, 'apenas_staging': 1},
    {'nome': 'paciente_racaCor_valor', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 1, 'distinct': 1, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'paciente_endereco_coIbgeMunicipio', 'tipo': 'int', 'tipo_pandas': pd.Int64Dtype(), 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'paciente_endereco_uf', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'paciente_endereco_cep', 'tipo': 'double precision', 'tipo_pandas': pd.Int64Dtype(), 'categorica': 0, 'distinct': 0, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'paciente_nacionalidade_enumNacionalidade', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'estabelecimento_valor', 'tipo': 'bigint', 'tipo_pandas': pd.Int64Dtype(), 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'estabelecimento_razaoSocial', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'estalecimento_noFantasia', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'estabelecimento_municipio_codigo', 'tipo': 'int', 'tipo_pandas': pd.Int64Dtype(), 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'estabelecimento_uf', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'vacina_grupoatendimento_codigo', 'tipo': 'int', 'tipo_pandas': pd.Int64Dtype(), 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'vacina_grupoAtendimento_nome', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 1, 'apenas_staging': 1},
    {'nome': 'vacina_categoria_codigo', 'tipo': 'double precision', 'tipo_pandas': pd.Int64Dtype(), 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 1},
    {'nome': 'vacina_categoria_nome', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 1, 'apenas_staging': 1},
    {'nome': 'vacina_dataAplicacao', 'tipo': 'date', 'tipo_pandas': 'date', 'categorica': 0, 'distinct': 1, 'trim': 0, 'apenas_staging': 0},
    {'nome': 'vacina_descricao_dose', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 1, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'vacina_nome', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 1, 'distinct': 1, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'vacina_lote', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 0, 'distinct': 0, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'sistema_origem', 'tipo': 'text', 'tipo_pandas': 'str', 'categorica': 1, 'distinct': 0, 'trim': 1, 'apenas_staging': 0},
    {'nome': 'data_importacao_rnds', 'tipo': 'date', 'tipo_pandas': 'date', 'categorica': 0, 'distinct': 0, 'trim': 0, 'apenas_staging': 0}
]

# 'document_id', 'paciente_id', 'paciente_idade',
#        'paciente_datanascimento', 'paciente_enumsexobiologico',
#        'paciente_racacor_codigo', 'paciente_racacor_valor',
#        'paciente_endereco_coibgemunicipio', 'paciente_endereco_copais',
#        'paciente_endereco_nmmunicipio', 'paciente_endereco_nmpais',
#        'paciente_endereco_uf', 'paciente_endereco_cep',
#        'paciente_nacionalidade_enumnacionalidade', 'estabelecimento_valor',
#        'estabelecimento_razaosocial', 'estalecimento_nofantasia',
#        'estabelecimento_municipio_codigo', 'estabelecimento_municipio_nome',
#        'estabelecimento_uf', 'vacina_grupoatendimento_codigo',
#        'vacina_grupoatendimento_nome', 'vacina_categoria_codigo',
#        'vacina_categoria_nome', 'vacina_lote', 'vacina_fabricante_nome',
#        'vacina_fabricante_referencia', 'vacina_dataaplicacao',
#        'vacina_descricao_dose', 'vacina_codigo', 'vacina_nome',
#        'sistema_origem', 'data_importacao_rnds', 'id_sistema_origem'

COLUNAS_VACINACAO = [dict((k, v.lower() if isinstance(v, str) else v) for k,v in col.items()) for col in COLUNAS_COMPLETA if not col['apenas_staging']]

COLUNAS_TODAS = [col['nome'] for col in COLUNAS_VACINACAO]

COLUNAS_CATEGORICAS = [col['nome'] for col in COLUNAS_VACINACAO if col['categorica']]

COLUNAS_GERAL = list(set(COLUNAS_TODAS) - set(COLUNAS_CATEGORICAS))

COLUNAS_DISTINCT = [('id_' if col['categorica'] else '') + col['nome'] for col in COLUNAS_VACINACAO if col['distinct']]

COLUNAS_DATA = [col['nome'] for col in COLUNAS_VACINACAO if col['tipo'] == 'date']

FIELD_TYPE_LIST_STAGING = {col['nome']: col['tipo_pandas'] for col in COLUNAS_COMPLETA if col['tipo'] not in ['date']}

FIELD_TYPE_LIST = {col['nome']: col['tipo_pandas'] for col in COLUNAS_VACINACAO if col['tipo'] not in ['date']}

# connection_uri = 'postgresql+psycopg2://usr_vacinas:sovacinasalva@localhost:5432/vacinas'
connection_uri = 'postgresql://usr_vacinas:sovacinasalva@192.168.15.4:5432/vacinas'
