#!/usr/bin/env python
# coding: utf-8

# In[1]:


from sqlalchemy import create_engine
import os
from os import listdir
from os.path import isfile, join
from datetime import datetime
import pandas as pd
import numpy as np

pd.set_option('display.max_rows', 6000)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 1000)

from settings import *


# In[2]:


engine = create_engine(connection_uri)


# In[3]:


sql = """
DROP TABLE IF EXISTS public.vacinacao_norm;
"""

# Executing SQL command
with engine.connect() as con:
    con.execute(sql)


# In[4]:


# sql_vacinacao = """
# CREATE TABLE public.vacinacao
# (
#     {cols}
# )
# """.format(cols = ",\n".join(['"{col}" {tipo}'.format(col=col['nome'], tipo=col['tipo']) for col in COLUNAS_VACINACAO]))

# print(sql_vacinacao)


# In[5]:


sql_vacinacao = """
CREATE TABLE public.vacinacao_norm
(
    {cols}, {cols_norm}
)
""".format(cols=',\n '.join(['"{col}" {tipo}'.format(col=col['nome'], tipo=col['tipo']) for col in COLUNAS_VACINACAO if col['nome'] not in COLUNAS_CATEGORICAS]),
                                               cols_norm=',\n '.join(['"id_{col}" SMALLINT'.format(col=col) for col in COLUNAS_CATEGORICAS])
          )
print(sql_vacinacao)


# In[6]:



# sql_vacinacao = """
# CREATE TABLE public.vacinacao
# (
#     paciente_id text,
#     paciente_idade text,
#     "paciente_dataNascimento" text,
#     "paciente_enumSexoBiologico" VARCHAR,
#     "paciente_racaCor_valor" text,
#     "paciente_endereco_coIbgeMunicipio" text,
#     paciente_endereco_uf text,
#     paciente_endereco_cep double precision,
#     "paciente_nacionalidade_enumNacionalidade" text,
#     estabelecimento_valor bigint,
#     "estabelecimento_razaoSocial" text,
#     "estalecimento_noFantasia" text,
#     estabelecimento_municipio_codigo text,
#     estabelecimento_uf text,
#     "vacina_grupoAtendimento_nome" text,
#     vacina_categoria_codigo double precision,
#     vacina_categoria_nome text,
#     "vacina_dataAplicacao" date,
#     vacina_descricao_dose text,
#     vacina_nome text,
#     sistema_origem text,
#     vacina_grupoAtendimento_nome bigint,
#     data_importacao_rnds date
# )
# WITH (
#     OIDS = FALSE
# )
# TABLESPACE pg_default;

# ALTER TABLE public.staging
#     OWNER to luizcelso;
# """


# In[7]:


# Executing SQL command
with engine.connect() as con:
    con.execute(sql_vacinacao)


# In[8]:


cols = [x.strip().split(" ")[0] for x in ''.join(sql_vacinacao.split("(")[1]).split(")")[0].split(',')]

cols_str = ', '.join(cols)

cols_str


# In[9]:


cols


# In[10]:


# sql_vacinacao_insert = """
# INSERT INTO vacinacao
# SELECT {} 
# FROM staging 
# --LIMIT 10
# ;
# """.format(cols_str)

# print(sql_vacinacao_insert)


# In[11]:


select_str = 'SELECT {cols}, {cols_norm}\n'.format(cols=', '.join(['staging."{col}"'.format(col=col) for col in COLUNAS_GERAL]),
                                               cols_norm=', '.join(['"{col}"."id_{col}"'.format(col=col) for col in COLUNAS_CATEGORICAS])
          )

select_str


# In[12]:


from_str = 'FROM staging {tables}\n'.format(tables=' '.join(['LEFT JOIN "{c}" ON (staging."{c}"="{c}"."{c}")'.format(c=x) for x in COLUNAS_CATEGORICAS])
          )
from_str


# In[13]:


insert_str = 'INSERT INTO vacinacao_norm ({cols}, {cols_norm})\n'.format(cols=', '.join(['"{col}"'.format(col=col) for col in COLUNAS_GERAL]),
                                               cols_norm=', '.join(['"id_{col}"'.format(col=col) for col in COLUNAS_CATEGORICAS])
          )
insert_str


# In[14]:


sql = """
    {insert}
    {select}
    ;
""".format(select=select_str+from_str, insert=insert_str)
print(sql)


# In[15]:



with engine.connect() as con:
    con.execute(sql)


# In[16]:




COLUNAS_GERAL


# ## Normaliza nomes de grupos prioritários e categorias

# In[17]:


sql = """
SELECT vacina_categoria_codigo, vacina_categoria_nome, vacina_grupoatendimento_codigo, vacina_grupoatendimento_nome, COUNT(*) AS total
FROM public.staging
GROUP BY vacina_categoria_codigo, vacina_categoria_nome, vacina_grupoatendimento_codigo, vacina_grupoatendimento_nome
ORDER BY total DESC
"""


# In[18]:


df = pd.read_sql(sql, engine)

df


# In[19]:


df.sort_values(['vacina_categoria_nome', 'vacina_grupoatendimento_nome'])


# In[20]:


# Descrição de código faltante
df[df.vacina_categoria_nome.isna() & ~df.vacina_categoria_codigo.isna()]


# In[21]:


# Preenche com filho
df.loc[df.vacina_categoria_nome.isna() & ~df.vacina_categoria_codigo.isna(), 'vacina_categoria_nome'] = df.loc[df.vacina_categoria_nome.isna() & ~df.vacina_categoria_codigo.isna(), 'vacina_grupoatendimento_nome']

df


# In[22]:


df_grupo = df[['vacina_grupoatendimento_codigo', 'vacina_grupoatendimento_nome', 'vacina_categoria_codigo']]

df_grupo


# In[23]:


df_grupo.sort_values(['vacina_grupoatendimento_codigo'])


# In[24]:


# Remove codigos duplicados com e sem descrição

def normaliza_nomes(grupo):
    return '|'.join(grupo.dropna())

df_grupo = df_grupo   .groupby(['vacina_grupoatendimento_codigo'])['vacina_grupoatendimento_nome']   .apply(normaliza_nomes).to_frame().reset_index()

df_grupo


# In[25]:


df_grupo.sort_values(['vacina_grupoatendimento_codigo'])


# In[26]:


df_grupo['vacina_grupoatendimento_nome'] = df_grupo.apply(lambda x: x['vacina_grupoatendimento_nome'] if x['vacina_grupoatendimento_nome'] else 'código: ' + str(x['vacina_grupoatendimento_codigo']), axis=1)

df_grupo


# In[27]:


df_grupo_cat = df[['vacina_grupoatendimento_codigo', 'vacina_categoria_codigo']]

df_grupo_cat = df_grupo_cat.dropna(subset=['vacina_categoria_codigo'])

df_grupo_cat = df_grupo_cat.drop_duplicates()

df_grupo_cat


# In[28]:


df_grupo_cat.value_counts().head()


# In[29]:


df_grupo = df_grupo.merge(df_grupo_cat, how='left')

df_grupo


# In[30]:


df_grupo.to_sql(
    name='vacina_grupoatendimento_nome',
    con=engine,
    if_exists='replace',
    index=False)


# In[31]:


df_cat = df[['vacina_categoria_codigo', 'vacina_categoria_nome']]

df_cat


# In[32]:


df_cat = df_cat.drop_duplicates().dropna()

df_cat


# In[33]:


df_cat.to_sql(
    name='vacina_categoria_nome',
    con=engine,
    if_exists='replace',
    index=False)


# In[ ]:




